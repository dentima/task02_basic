package com.dentima;
import java.util.Scanner;

/**
 * A class that describes the interval and its actions.
 */
public class Interval {
    /**
     * Start point in the interval.
     */
    private int startPoint;
    /**
     * End point in the interval.
     */
    private int endPoint;
    /**
     * Max odd element in the interval.
     */
    private int maxOdd = 0;
    /**
     * Max even element in the interval.
     */
    private int maxEven = 0;

    /**
     * Constructor without arguments.
     */
    public Interval() {
    }

    /**
     *
     * @param startPoint Start point in the interval.
     * @param endPoint End point in the interval.
     */
    public Interval(final int startPoint, final int endPoint) {
        this.startPoint = startPoint;
        this.endPoint = endPoint;
        checkMax();
    }

    /**
     * Function for input value for start and end points.
     */
    public final void inputInterval() {
        System.out.println("Enter start point");
        Scanner scanner = new Scanner(System.in);
        startPoint = scanner.nextInt();
        System.out.println("Enter end point");
        endPoint = scanner.nextInt();
        System.out.println("Entered interval: "
                + "[" + startPoint
                + "," + endPoint + "]");
        checkMax();
    }

    /**
     * Function update in maxEven and maxOdd.
     */
    public final void checkMax() {
        if (endPoint % 2 == 0) {
            maxEven = endPoint;
            maxOdd = endPoint - 1;
        } else {
            maxOdd = endPoint;
            maxEven = endPoint - 1;
        }
    }

    /**
     * Function print odd numbers of fibonacci.
     */
    public final void printOddNumbers() {
        System.out.println("All odd numbers in the interval");
        int sumOddNumbers = 0;
        for (int i = (startPoint % 2 == 0) ? (startPoint + 1) : startPoint;
             i <= endPoint; i += 2) {

            sumOddNumbers += i;
            System.out.print(i + ", ");
        }
        System.out.println("Sum all odd numbers = " + sumOddNumbers);
    }

    /**
     * Function print odd numbers of fibonacci.
     */
    public final void printEvenNumbers() {
        System.out.println("All even numbers in the interval");
        int sumEvenNumbers = 0;
        for (int i = (endPoint % 2 == 0) ? endPoint : (endPoint - 1);
             i >= startPoint; i -= 2) {

            sumEvenNumbers += i;
            System.out.print(i + ", ");
        }
        System.out.println("Sum all even numbers = " + sumEvenNumbers);
    }

    /**
     *
     * @return Max odd element in the interval.
     */
    public final int getMaxOdd() {
        return maxOdd;
    }

    /**
     *
     * @return Max even element in the interval.
     */
    public final int getMaxEven() {
        return maxEven;
    }
}
