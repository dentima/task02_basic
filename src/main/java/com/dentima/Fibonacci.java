package com.dentima;

/**
 * A class that describes the numbers Fibonacci and its actions.
 */
public class Fibonacci {
    /**
     * fibonacci array.
     */
    private int[] fibonacci;
    /**
     * odd numbers.
     */
    private int oddNumbers;
    /**
     * even numbers.
     */
    private int evenNumbers;
    /**
     * size fibonacci numbers.
     */
    private final int size;
    /**
     * variable percent.
     */
    private final int percent;

    /**
     *
     * @param size Size of fibonacci numbers
     * @param firstNumber First number of fibonacci numbers
     * @param secondNumber Second numbers of fibonacci numbers
     */
    public Fibonacci(final int size, final int firstNumber,
        final int secondNumber) {
        percent = 100;

        this.size = size;

        fibonacci = new int[size];
        fibonacci[0] = firstNumber;
        fibonacci[1] = secondNumber;

        for (int i = 2; i < size; i++) {
            fibonacci[i] = fibonacci[i - 2] + fibonacci[i - 1];
        }

        oddNumbers = 0;
        evenNumbers = 0;

        calcPercentage();
    }

    /**
     * Method calculates how many even and odd
     * numbers.
     */
    private void calcPercentage() {
        for (int i = 0; i < size; i++) {
            if (fibonacci[i] % 2 == 0) {
                evenNumbers++;
            } else {
                oddNumbers++;
            }
        }
    }

    /**
     *
     * @return array of fibonacci numbers.
     */
    public final int[] getFibonacci() {
        return fibonacci;
    }

    /**
     *
     * @return String percentage fibonacci odd and even numbers.
     */
    public final String printResult() {
        return ("Odd Fibonacci numbers "
                + oddNumbers / size * percent
                + "%" + ". Even Fibonaci numbers "
                + evenNumbers / size * percent + "%");
    }
}
