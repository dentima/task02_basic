package com.dentima;

import java.util.Arrays;
import java.util.Scanner;

/**
 * The main program class.
 */
public class Application {
    /**
     * Program build Fibonacci numbers: F1 will be the biggest odd number
     * and F2 – the biggest even number, user can enter the size of set.
     * @author Denys Tymoshchuk
     * @param args Enter param from command line
     */
    public static void main(final String[] args) {
        int size = 0;
        Interval interval = new Interval();
        Scanner scanner = new Scanner(System.in);

        interval.inputInterval();
        interval.printOddNumbers();
        interval.printEvenNumbers();

        System.out.println("Enter size Fibonacci");
        size = scanner.nextInt();

        Fibonacci fibonacci = new Fibonacci(
                size, interval.getMaxOdd(), interval.getMaxEven());
        System.out.println("Fibonacci numbers: "
                + Arrays.toString(fibonacci.getFibonacci()));
        System.out.println(fibonacci.printResult());
    }
}
